
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Pokemon {
    String nome;
    String tipo1;
    String tipo2;
    String ability1;
    String ability2;
    String ability3;
    String move1;
    String move2;
    String move3;
    String move4;
    String move5;
    String move6;
    String move7;
    String image;
    
    public Pokemon(){
        nome = "Não Encontrado";
        tipo1 = "Não Possui";
        tipo2 = "Não Possui";
        ability1 = "Não Possui";
        ability2 = "Não Possui";
        ability3 = "Não Possui";
        move1 = "Não Possui";
        move2 = "Não Possui";
        move3 = "Não Possui";
        move4 = "Não Possui";
        move5 = "Não Possui";
        move6 = "Não Possui";
        move7 = "Não Possui";
        image = "";
    }
    
    public String getnome(){
        return nome;
    }
    public void setnome(String nome){
        this.nome = nome;
    }
    public String gettipo1(){
        return tipo1;
    }
    public void settipo1(String tipo1){
        this.tipo1 = tipo1;
    }
    public String gettipo2(){
        return tipo2;
    }
    public void settipo2(String tipo2){
        this.tipo2 = tipo2;
    }
    public String getability1(){
        return ability1;
    }
    public void setability1(String ability1){
        this.ability1 = ability1;
    }
    public String getability2(){
        return ability2;
    }
    public void setability2(String ability2){
        this.ability2 = ability2;
    }
    public String getability3(){
        return ability3;
    }
    public void setability3(String ability3){
        this.ability3 = ability3;
    }
    public String getmove1(){
        return move1;
    }
    public void setmove1(String move1){
        this.move1 = move1;
    }
    public String getmove2(){
        return move2;
    }
    public void setmove2(String move2){
        this.move2 = move2;
    }public String getmove3(){
        return move3;
    }
    public void setmove3(String move3){
        this.move3 = move3;
    }public String getmove4(){
        return move4;
    }
    public void setmove4(String move4){
        this.move4 = move4;
    }public String getmove5(){
        return move5;
    }
    public void setmove5(String move5){
        this.move5 = move5;
    }public String getmove6(){
        return move6;
    }
    public void setmove6(String move6){
        this.move6 = move6;
    }public String getmove7(){
        return move7;
    }
    public void setmove7(String move7){
        this.move7 = move7;
    }
    public String image(){
        return image;
    }
    public void image(String image){
        this.image = image;
    }
}
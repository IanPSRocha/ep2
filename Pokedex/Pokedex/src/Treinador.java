
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class Treinador {
    String id;
    String idade;
    String sexo;
    
    public Treinador(){
        id = "Nulo";
        idade = "0";
        sexo = "Indefinido";
    }
    public Treinador(String id, String idade, String sexo){
        this.id = id;
        this.idade = idade;
        this.sexo = sexo;
    }
    public String getid(){
        return id;
    }
    public void setid(String id){
        this.id = id;
    }
    public String getsidade(){
        return idade;
    }
    public void setidade(String idade){
        this.idade = idade;
    }
    public String getsexo(){
        return sexo;
    }
    public void setsexo(String sexo){
        this.sexo = sexo;
    }
    public void RegistroTreinador() {
		try
	    {
	        BufferedWriter csvWriter = new BufferedWriter(new FileWriter("/home/ian/Orien. Obj/ep2/data/csv_files/Treinadores.csv", true));

	        csvWriter.append(id);
	        csvWriter.append(',');
	        csvWriter.append(idade);
	        csvWriter.append(',');
	        csvWriter.append(sexo);
	        csvWriter.append('\n');

	        csvWriter.flush();
	        csvWriter.close();
	    }
	    catch(IOException e)
	    {
	         e.printStackTrace();
	    } 
	}
}
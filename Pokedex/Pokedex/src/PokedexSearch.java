
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class PokedexSearch {
    
    private ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();
    
    public PokedexSearch(){
        
    }
    
    public ArrayList<Pokemon> getPokemons(){
        return pokemons;
    }
    
    public void readPokemonData() {
	String csvFile;
	String line;
	String csvSplitBy = ",";
        
        csvFile = "/home/ian/Orien. Obj/ep2/data/csv_files/POKEMONS_DATA_2.csv";
            
        try(BufferedReader br2 = new BufferedReader(new FileReader(csvFile))){
            	
        int counter = 0;
           
        while((line = br2.readLine()) != null) {
        
        String[] pokemonAtributeFromSecondCSV = line.split(csvSplitBy);
            		
        for(int i = 1; i < pokemonAtributeFromSecondCSV.length; i++) {
            switch(i) {
                case 1:
                    pokemons.get(counter).setnome(pokemonAtributeFromSecondCSV[1]);
                    break;
            	case 2:
                    break;
            	case 3:
                    break;
            	case 4:
                    break;
            	case 5:
                    pokemons.get(counter).setability1(pokemonAtributeFromSecondCSV[5]);
                    break;
            	case 6:
                    pokemons.get(counter).setability2(pokemonAtributeFromSecondCSV[6]);
                    break;
            	case 7:
                    pokemons.get(counter).setability3(pokemonAtributeFromSecondCSV[7]);
                    break;
            	case 8:
                    pokemons.get(counter).setmove1(pokemonAtributeFromSecondCSV[8]);
                    break;
            	case 9:
                    pokemons.get(counter).setmove2(pokemonAtributeFromSecondCSV[9]);
                    break;
            	case 10:
                    pokemons.get(counter).setmove3(pokemonAtributeFromSecondCSV[10]);
                    break;
            	case 11:
                    pokemons.get(counter).setmove4(pokemonAtributeFromSecondCSV[11]);
                    break;
            	case 12:
                    pokemons.get(counter).setmove5(pokemonAtributeFromSecondCSV[12]);
                    break;
            	case 13:
                    pokemons.get(counter).setmove6(pokemonAtributeFromSecondCSV[13]);
                    break;
            	case 14:
                    pokemons.get(counter).setmove7(pokemonAtributeFromSecondCSV[14]);
                    break;
            }
        }
        counter++;
        }
        }catch(IOException e) {
  	    e.printStackTrace();          
        }       
    }
}
